import axios from 'axios';

const storageKey = 'frapp-db';

import * as firebase from 'firebase/app';

import "firebase/database";

// Database implemented using Firebase Realtime Database and REST requests via axios library

class Database {
    constructor(rootPath, stateChange) {
        this._rootPath = rootPath;
        this._state = {ready: false, state: 'notOpen'}
        this._stateChange = stateChange;

        let cachedDbString = localStorage.getItem(storageKey);
        if (cachedDbString != null) {
            this._data = JSON.parse(cachedDbString);
            this._state = {ready: true, state: 'fromLocalCache'};
        }

        this._dbRef = firebase.database().ref(rootPath);

        this._dbRef.once('value') // one-off load of entire db
            .then(snapshot => {
                this._data = snapshot.val();
                localStorage.setItem(storageKey, JSON.stringify(this._data));
                this._state = {ready: true, state: 'ready'};
                this._stateChange(this._state);                
            })
            .catch(error => {
                this._state = {ready: false, state: 'dbProblem: ' + error.code};
                this._stateChange(this._state); 
            });
    }

    getState() {
        return this._state;
    }

    init(idToken) {
        this._state.state = 'loading';
        this._stateChange(this._state);
        this._idToken = idToken;

        axios({
            url: `${this._rootPath}.json`,
            params: { "auth": this._idToken }
        })
        .then(response => {
            this._data = response.data;
            localStorage.setItem(storageKey, JSON.stringify(this._data));
            this._state = {ready: true, state: 'ready'};
            this._stateChange(this._state);
        })
        .catch(error => {
            // probably a 403... TODO: check for other outcomes here
            this._state = {ready: false, state: 'accessDenied'};
            this._stateChange(this._state);
        })
    }

    // methods to search, update etc. here
    
    getAllItems() {
        return this._data.items;
    }

    getAllLocations() {
        // console.log('getting locations');
        return this._data.locations;
    }

    searchItems(searchTerm) {
        if (searchTerm === '')
            return [];
        else
            return Object.entries(this._data.items)
                .filter(([id, item]) => 
                    item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0
                );
    }

    getItemByBarcode(barcode) {
        return Object.entries(this._data.items)
            .filter(([id, item]) => item.barcodes != null && item.barcodes.includes(barcode));       
    }

    addItem(item) {
        return new Promise((successCb, errorCb) => {
            // update cache
            this._data.items['_tempId'] =item;
            window.localStorage.setItem(storageKey, JSON.stringify(this._data));
            // update database
            axios({
                method: 'POST',
                url: `${this._rootPath}/items.json`,
                params: { "auth": this._idToken },
                data: item
            })
            .then(response => {
                console.log('POST response', response)
                let newId = response.data.name;
                this._data.items[newId] = this._data.items['_tempId'];
                delete this._data.items['_tempId'];
                window.localStorage.setItem(storageKey, JSON.stringify(this._data));
                successCb(newId);
            })
            .catch(error => {
                console.log('POST error', error);
                errorCb(error);
            })
        });
    }

    updateItem(id, item) {
        // update cache
        this._data.items[id] = item;
        window.localStorage.setItem(storageKey, JSON.stringify(this._data));
        // update database
        if (id != '_tempId') {
            axios({
                method: 'PUT',
                url: `${this._rootPath}/items/${id}.json`,
                params: { "auth": this._idToken },
                data: item
            })
            .then(response => {
                console.log('PUT response', response)
            })
            .catch(error => {
                console.log('PUT error', error)
            })
        }

    }

}

window.firebase = firebase;

export default Database;