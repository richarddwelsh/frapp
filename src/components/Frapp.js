import React from 'react';
import ReactDOM from 'react-dom';
import Database from '../Database.js';

import FirebaseAuth from '../FirebaseAuth.js'
import ItemBrowser from './ItemBrowser.js';

// import './Frapp.css';

class Frapp extends React.Component {

    constructor() {
        super();

        const searchParams = new URLSearchParams(window.location.search);
        this.barcode = searchParams.get('code');

        this.auth = new FirebaseAuth(
            user => { 
                this.setState({ user: user, signedIn: true });
                // console.log(user.displayName); 
            },
            idToken => { 
                // don't need to store the token here bacuse it'll be stored inside the instance of Database
                // this.db.init(idToken);
            },
            () => {
                this.setState({ signedIn: false, user: null });
            }
        );
        this.auth.init();

        this.db = new Database(
            'frapp/', 
            newState => { this.dbStateChange(newState); }
            );

        this.state = {
            signedIn: false,
            dbState: this.db.getState(),
            search: searchParams,
            searchTerm: searchParams.get('s')
        }
    }

    dbStateChange(newState) {
        this.setState({dbState: newState});
    }

    handleSearchChange(searchTerm) {
        this.setState(state => {
            state.search.set('s', searchTerm);
            window.history.pushState(searchTerm, null, '?' + state.search.toString());

            return {
                search: state.search,
                searchTerm: searchTerm
            }
        });
    }

    componentDidMount() {
        
    }

    render() {     
        return (
            <div id="app-root">
                {this.state.signedIn ?
                    <div>
                        {this.state.dbState.ready ?
                            <ItemBrowser db={this.db} barcode={this.barcode} searchTerm={this.state.searchTerm} searchTermChange={s => this.handleSearchChange(s)} />
                            :
                            <img src="favicon.ico" />
                        }
        
                        <div id='footer'>
                            <p>Signed in as {this.state.user.displayName} <input type="button" value="Sign out" onClick={this.auth.signOut} /></p>
                            <p>Database: {this.state.dbState.state} {this.barcode && `| Barcode: ${this.barcode}`}</p>
                        </div>
                    </div>
                    :
                    <img src="favicon.ico" />
                }
            </div>
            );
    }
}

ReactDOM.render(<Frapp/>, document.getElementById('root'));