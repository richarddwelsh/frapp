import React from 'react';
import Item from './Item.js';

class ItemBrowser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchTerm: this.props.searchTerm || '',
            searchSelection: this.props.searchTerm ? this.props.db.searchItems(this.props.searchTerm) : [],
            barcodeSelection: this.props.barcode ? props.db.getItemByBarcode(this.props.barcode) : null
            };
        this.locations = props.db.getAllLocations();

        this.handleSearchChange = this.handleSearchChange.bind(this);
    }

    handleSearchChange(event) {
        let searchTerm = event.target.value;

        this.setState({
            searchTerm: searchTerm,
            searchSelection: this.props.db.searchItems(searchTerm)
        });

        this.props.searchTermChange(searchTerm);
    }

    itemDataChange(id, item) {
        if (id != '_new') {
            this.props.db.updateItem(id, item);
            this.setState({
                // in case a barcode has been assigned to an item...
                barcodeSelection: this.props.barcode ? this.props.db.getItemByBarcode(this.props.barcode) : null,
                searchSelection: this.props.db.searchItems(this.state.searchTerm)
            });
            }
        else {
            this.props.db.addItem(item)
                .then(newId => {
                    this.setState({
                        // in case a barcode has been assigned to an item...
                        barcodeSelection: this.props.barcode ? this.props.db.getItemByBarcode(this.props.barcode) : null,
                        searchSelection: this.props.db.searchItems(this.state.searchTerm)
                    });            
                });
        }

    }

    unknownBarcode() {
        return this.state.barcodeSelection && this.state.barcodeSelection.length == 0;
    }

    render() {
        return (
            <div id='item-browser' className='comp'>
                                    
                <input type='text' value={this.state.searchTerm} onChange={this.handleSearchChange} placeholder="Frapp search" className='search-field' />

                <div id='item-list'>
                    {this.state.barcodeSelection && this.state.barcodeSelection.map(([key, value]) =>
                        <Item key={key} datum={value} 
                            locs={this.locations} 
                            onChange={item => this.itemDataChange(key, item)} />
                    )}

                    {this.state.searchSelection.map(([key, value]) =>
                        <Item key={key} datum={value} 
                            locs={this.locations} 
                            newBarcode={this.unknownBarcode() ? this.props.barcode : null}
                            onChange={item => this.itemDataChange(key, item)} />
                    )}

                    {((this.state.searchSelection.length == 0 || this.unknownBarcode())  && this.state.searchTerm != '') &&
                        <Item key='_new' datum={{name: this.state.searchTerm, totals: {}}} new={true}  
                            locs={this.locations}
                            newBarcode={this.unknownBarcode() ? this.props.barcode : null}
                            onChange={item => this.itemDataChange('_new', item)} />
                    }
                </div>

            </div>            
        );
    }
}

export default ItemBrowser;