import React from 'react';
import StockCounter from './StockCounter.js';
import Square from './Square.js';

const icons = ['box', 'bottle', 'tin', 'pack', 'jar', 'carton'];

class Item extends React.Component{
    constructor(props) {
        super(props);

        this.datum = this.props.datum;
        this.datum.hasBarcode = hasBarcode;

        
    }

    render() {
        this.datum = this.props.datum;
        this.datum.hasBarcode = hasBarcode;
        
        return (
            <div className={`comp item ${this.props.new ? ' new' : ''}`}>
                {(this.props.new && <div className='flag'>new</div>)}
                
                <div className='name'> {/*<Square size="1em" />*/} 
                    <span className='text'>{this.props.datum.name}</span>
                    {this.props.datum.unit && icons.includes(this.props.datum.unit) && <img src={`images/${this.props.datum.unit}.svg`} className='icon' />}
                    {(this.props.newBarcode != null && !this.datum.hasBarcode(this.props.newBarcode)) && 
                    <input type='button' value={this.props.new ? 'add+assign' : '+Barcode'} onClick={() => this.assignBarcode()} /> }
                </div>
                
                <div className='stock-levels'>
                    {Object.entries(Object.assign({}, this.props.locs, this.props.datum.totals || {}))
                        .map(([key, value]) => 
                        <StockCounter key={key} val={value} label={key} onChange={(l, q) => this.stockLevelChange(l, q)} />
                    )}
                </div>

            </div>
        )
    }

    stockLevelChange(loc, quantity) {
        this.datum.totals = this.datum.totals || {[loc]: quantity};
        this.datum.totals[loc] = quantity;

        this.props.onChange(this.datum);
    }

    assignBarcode() {
        this.datum.barcodes = this.datum.barcodes || [];
        this.datum.barcodes.push(this.props.newBarcode);

        this.props.onChange(this.datum);
    }
}

function hasBarcode(barcode) {
    return Array.isArray(this.barcodes) && this.barcodes.includes(barcode);
}

export default Item;