import React from 'react';

class StockCounter extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            value: props.val == -1 ? 0 : props.val,
            allowNotApplicable: (props.val == -1)
        };
    }

    render() {
        if (this.props.val == 'n/a')
            return null;
        else
            return (
                <div className="comp stock-counter">
                    <span className="label">{this.props.label}
                        {/* {(this.state.allowNotApplicable && this.state.value == 0) && 
                            <input type="button" value="n/a" onClick={() => this.setNotApplicable()} />
                        } */}
                    </span>
                    <div className="adjuster">
                        <input type="button" className="decr" value="-" onClick={() => {this.adjust(-1)}} disabled={this.state.value <= 0} />
                        <span className="value">{this.state.value}</span>
                        <input type="button" className="incr" value="+" onClick={() => this.adjust(1)} />
                    </div>
                </div>
            );
    }
    
    adjust(delta) {
        this.setState((state, props) => {
            let newValue = state.value + delta;
            props.onChange(this.props.label, newValue);
            return {
                value: newValue
            };
        });
    }

    setNotApplicable() {
        this.setState({
            value: 'n/a'
        });
        this.props.onChange(this.props.label, 'n/a');
    }
}

export default StockCounter;