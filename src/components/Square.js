import React from 'react';

class Square extends React.Component {

    render() {
        return (
            <svg width={this.props.size} height={this.props.size} viewBox="0 0 100 100">
                <rect x="0" y="0" width="100" height="100" fill="none" stroke="black" strokeWidth="5" />
            </svg>
        );
    }
}

export default Square;