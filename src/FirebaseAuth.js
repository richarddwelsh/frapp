import * as firebase from 'firebase/app';

import "firebase/auth";

class FirebaseAuth {
    constructor(gotUser, gotToken, notSignedIn) {
        this.config = {
            apiKey: "AIzaSyCl3Ps-C1RyUmB6O4GM78WgCMvT7eqccjI",
            authDomain: "welshdesign-data.firebaseapp.com",
            databaseURL: "https://welshdesign-data.firebaseio.com",
            projectId: "welshdesign-data",
            storageBucket: "",
            messagingSenderId: "133278548349",
            appId: "1:133278548349:web:e21fd78e1040c3e8"
        };

        // handlers
        this.gotUser = gotUser;
        this.gotToken = gotToken;
        this.notSignedIn = notSignedIn;
    }

    init() {
        if (!firebase.apps.length) {
            firebase.initializeApp(this.config);
        }
        
        let googleAuth = new firebase.auth.GoogleAuthProvider();
        let twitterAuth = new firebase.auth.TwitterAuthProvider();

        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.gotUser(user);

                user.getIdToken(true).then(idToken => {
                    console.log(idToken);
                    this.gotToken(idToken);
                })
            } else {
                if (typeof this.notSignedIn === "function")
                    this.notSignedIn();
                    
                firebase.auth().signInWithRedirect(googleAuth);
            }
        });
    }

    signOut() {
        firebase.auth().signOut();
    }

}

export default FirebaseAuth;